import { Component, ViewChild, ElementRef , OnInit} from '@angular/core';
import { AssociationService } from 'src/app/association.service';
import {Observable} from 'rxjs';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';
import {NgbDate, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import {jsPDF} from 'jspdf';
import html2canvas from 'html2canvas';
  
   
@Component({
   selector: 'app-root',
   templateUrl: './app.component.html',
   styleUrls: ['./app.component.css']
})


export class AppComponent  implements OnInit   {
   @ViewChild('htmlData') htmlData:ElementRef;
   @ViewChild('transaactionData') transData:ElementRef;
   hoveredDate: NgbDate | null = null;
   fromDate: NgbDate;
   toDate: NgbDate | null = null;   
      elementType = NgxQrcodeElementTypes.URL;
      correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;
      value = 'https://www.techiediaries.com/';
      rowData: Observable<any[]>;
      rowAllData: Observable<any[]>;
      rowFiltedrData: Observable<any[]>;
      private gridApi:any;
      pdfSenderAccountName: any = "";
      pdfSenderAccountNumber: any = "";
      pdfReceiverAccountName: any = "";
      pdfReceiverAccountNumber: any = "";
      pdfTransactionId: any = "";
      pdfTransactionType: any = "";
      pdfDateTime: any = "";
      pdfAmount: any = "";
      USERDETAILS  = [{key1:"Account Name",val1:"Mr, Krishna Moorthy", key2:"Period",val2:"All"},
      {key1:"Account Number",val1:"+95 09798039885", key2:"Total Cashback Amount",val2:"550 MMK"},
      {key1:"E-mail",val1:" krishnamoorthytaurus@gmail.com ,krishnamoorthy@goautoticket .com", key2:"Total Credit Amount",val2:"0 MMK"},
      {key1:"Account Type",val1:"Personal", key2:"Total Debit Amount",val2:"435,000 MMK"},
      {key1:"Address",val1:" No (7) Ward, Kamayut Township, Yangon, Yangon Division", key2:"Total Transaction Amount",val2:"435,000 MMK"},
      {key1:"",val1:"", key2:"Closing Balance",val2:"2,666 MMK"},
      {key1:"",val1:"", key2:"Date & Time",val2:"Thu, 02-Sep-2021 11:34:22"},
      {key1:"",val1:"", key2:"Total Transactions",val2:"5"}];
      pdfColumns =['Transaction ID','Mobile Number','Cash Back (MMK)','Opening Balance (MMK)','Credit (MMK)','Debit (MMK)', 'Closing Balance (MMK)', 'Date & Time'];
      pdfData:any[];
   columnDefs = [

      { field: 'TransactionID', sortable: true, filter: true, unSortIcon: true, sort: 'desc',
      width: 350,
      wrapText: true,cellRenderer: function(params:any) {
         return  `<div><span>${params.value}</span><br/><span>${params.data.Comments}</span></div>`;
     }
   },
      { field: 'Amount',width: 125, sortable: true, filter: true, unSortIcon: true },
      { field: 'Destination',width: 200, sortable: true, filter: true, unSortIcon: true, headerName: "Mobile Number",cellRenderer: function(params:any) {
         
         let newLink = "";
         if(params.data.TransactionTypeName == "PAYTO"){ 
            newLink = `<div><img src="../../assets/Images/get.png" alt="Myanmar Flag" /> <br /><img src="../../assets/Images/flags/myanmar_flag.png" alt="Myanmar Flag" /><span>${params.value}</span> <span class="flt-non">${params.data.TownshipName}</span></div>`;
         } else{
            newLink = `<div><img src="../../assets/Images/blu.png" alt="Myanmar Flag" /> <br /><img src="../../assets/Images/flags/myanmar_flag.png" alt="Myanmar Flag" /><span>${params.value}</span> <span class="flt-non">${params.data.TownshipName}</span></div>`;
         }
         return newLink;
       } }, 
      { field: 'BusinessName', width: 180, sortable: true, filter: true, unSortIcon: true, textAlign: "center" },
      { field: 'AccountType', width: 160, sortable: true, filter: true, unSortIcon: true },
      { field: 'TransactionTypeName', width: 170, sortable: true, filter: true, unSortIcon: true, headerName: "Payment Type" },
      { field: 'BonusPoint',width: 170, sortable: true, filter: true, unSortIcon: true, headerName: "Bonus Points" },
      { field: 'KickBack', width: 170,sortable: true, filter: true, unSortIcon: true, headerName:"Cash Back" },
      { field: 'Balance', width: 130,sortable: true, filter: true, unSortIcon: true },
      { field: 'Updated_Date', sortable: true, filter: true, unSortIcon: true, headerName: "Date & Time",
      width: 350,
      wrapText: true,cellRenderer: function(params:any) {
         return  `<div><span>${params.value}</span><br/><img src="../../assets/Images/pdf_icon.png" class="export-icon" alt="Export to PDF" name="exportpdf" /><img class="export-icon" src="../../assets/Images/qrcode_icon.png" alt="QR Code" name="qrcode" /></div>`;
         
     }
   }
   ];
   rowHeight: any;
   BankPhoneNumber = "";

   constructor(private assoc: AssociationService, private route: ActivatedRoute, private calendar: NgbCalendar) {
      this.fromDate = calendar.getToday();
      this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
      console.log("Constructor Called !");
    }

   
   reqData = {"Login":{"MobileNumber":"00959783798314","Simid":"89950615110155751690","Msid":"414061115575169","Ostype":0,"Otp":null,"AppId":null,"Limit":0,"Offset":0,"PhoneBrand":null,
   "PhoneModel":null,"PhoneOsVersion":null,"Imei":null,"Simid2":null,"Msid2":null,"DeviceId":null,"AppBuildName":null,"AppBuildnumber":null,"AppPackageName":null},
   "StartDate":"2018-01-01","EndDate":"2021-10-09","Limit":0,"OffSet":0,
   "UserDetails":{"AccountName":"Mr, Krishna Moorthy", "AccountNumer":"(+95)09798039885","Email":"krishnamoorthytaurus@gmail.com,krishnamoorthy@goautoticket .com", "AccountType":"", "Address":"No (7) Ward, Kamayut Township, Yangon, Yangon Division"}};

   //  reqData = {"Login":{"MobileNumber":"00959783798314","Simid":"89950615110155751690","Msid":"414061115575169","Ostype":0,"Otp":null,"AppId":null,"Limit":0,"Offset":0,"PhoneBrand":null,
   // "PhoneModel":null,"PhoneOsVersion":null,"Imei":null,"Simid2":null,"Msid2":null,"DeviceId":null,"AppBuildName":null,"AppBuildnumber":null,"AppPackageName":null},
   // "StartDate":"2018-01-01","EndDate":"2021-10-09","Limit":0,"OffSet":0};
   serverSideDatasource = [];
   ngOnInit(): void {
      console.log("Enter into Ng Init Function!");
      try{
      document.getElementsByClassName("export-icon");
      this.route.queryParams.subscribe(params => {
        console.log(params); 

        if (typeof params.StartDate != "undefined" && params.EndDate != "undefined") {
         
         this.reqData.StartDate = params.StartDate;
         this.reqData.EndDate = params.EndDate;
         this.reqData.Login = params.Login;
         this.reqData.Limit = params.Limit;
         this.reqData.OffSet = params.OffSet;
         this.reqData.UserDetails = params.UserDetails;
        }
      }
    );

      this.rowHeight = 100;
      //let respData = JSON.parse('[{"Source":"00959783798314","Destination":"00959266277759","TransactionTypeName":"PAYTO","Amount":10.0000,"Balance":0.0,"TransactionID":"2009506274","TransactionTime":"2021-06-16T20:42:13","Updated_Date":"6/16/2021 8:41:04 PM","Latitude":"","Longitude":"","CellTowerID":"","ResultCode":0,"ResultDescription":"Transaction Successful","Is_Online":false,"SelectionMode":false,"Is_Confirmed":false,"KickBack":0.0000,"BonusPoint":0,"Comments":"Bank To Wallet , Bank TransactionId : 2349591 Account Number :00959266277759  ","TownshipName":null,"TownshipId":null,"DestinationNumberWalletBalance":0.0000,"RootAgentCode":"00959783798314","CashFlowType":"0"},{"Source":"00959783798314","Destination":"00959973332668","TransactionTypeName":"PAYTO","Amount":2000.0000,"Balance":0.0,"TransactionID":"2009505832","TransactionTime":"2021-06-03T23:07:58","Updated_Date":"6/3/2021 11:06:50 PM","Latitude":"","Longitude":"","CellTowerID":"","ResultCode":0,"ResultDescription":"Transaction Successful","Is_Online":false,"SelectionMode":false,"Is_Confirmed":false,"KickBack":0.0000,"BonusPoint":0,"Comments":"Bank To Wallet , Bank TransactionId : 012106032306444152 Account Number :1234  ","TownshipName":null,"TownshipId":null,"DestinationNumberWalletBalance":247615.0000,"RootAgentCode":"00959783798314","CashFlowType":"0"},{"Source":"00959783798314","Destination":"00959262888032","TransactionTypeName":"PAYTO","Amount":1.0000,"Balance":0.0,"TransactionID":"2009506230","TransactionTime":"2021-06-16T16:16:19","Updated_Date":"6/16/2021 4:15:09 PM","Latitude":"","Longitude":"","CellTowerID":"","ResultCode":0,"ResultDescription":"Transaction Successful","Is_Online":false,"SelectionMode":false,"Is_Confirmed":false,"KickBack":0.0000,"BonusPoint":0,"Comments":"Bank To Wallet , Bank TransactionId : UABTESTID001 Account Number :1234  ","TownshipName":null,"TownshipId":null,"DestinationNumberWalletBalance":39029751.0600,"RootAgentCode":"00959783798314","CashFlowType":"0"},{"Source":"00959783798314","Destination":"00959973332668","TransactionTypeName":"PAYTO","Amount":10000.0000,"Balance":0.0,"TransactionID":"2009505828","TransactionTime":"2021-06-03T16:12:42","Updated_Date":"6/3/2021 4:11:35 PM","Latitude":"","Longitude":"","CellTowerID":"","ResultCode":0,"ResultDescription":"Transaction Successful","Is_Online":false,"SelectionMode":false,"Is_Confirmed":false,"KickBack":0.0000,"BonusPoint":0,"Comments":"Bank To Wallet , Bank TransactionId : 012106031611284148 Account Number :1234  ","TownshipName":null,"TownshipId":null,"DestinationNumberWalletBalance":242615.0000,"RootAgentCode":"00959783798314","CashFlowType":"0"},{"Source":"00959783798314","Destination":"00959973332668","TransactionTypeName":"PAYTO","Amount":2000.0000,"Balance":0.0,"TransactionID":"2009505822","TransactionTime":"2021-06-03T15:37:08","Updated_Date":"6/3/2021 3:36:02 PM","Latitude":"","Longitude":"","CellTowerID":"","ResultCode":0,"ResultDescription":"Transaction Successful","Is_Online":false,"SelectionMode":false,"Is_Confirmed":false,"KickBack":0.0000,"BonusPoint":0,"Comments":"Bank To Wallet , Bank TransactionId : 012106031535504143 Account Number :1234  ","TownshipName":null,"TownshipId":null,"DestinationNumberWalletBalance":219615.0000,"RootAgentCode":"00959783798314","CashFlowType":"0"},{"Source":"00959783798314","Destination":"00959262888032","TransactionTypeName":"PAYTO","Amount":10.0000,"Balance":0.0,"TransactionID":"2009506278","TransactionTime":"2021-06-16T20:48:32","Updated_Date":"6/16/2021 8:47:22 PM","Latitude":"","Longitude":"","CellTowerID":"","ResultCode":0,"ResultDescription":"Transaction Successful","Is_Online":false,"SelectionMode":false,"Is_Confirmed":false,"KickBack":0.0000,"BonusPoint":0,"Comments":"Bank To Wallet , Bank TransactionId : 2349592 Account Number :00959262888032  ","TownshipName":null,"TownshipId":null,"DestinationNumberWalletBalance":39029761.0600,"RootAgentCode":"00959783798314","CashFlowType":"0"},{"Source":"00959783798314","Destination":"00959266277759","TransactionTypeName":"PAYTO","Amount":10.0000,"Balance":0.0,"TransactionID":"2009506276","TransactionTime":"2021-06-16T20:42:17","Updated_Date":"6/16/2021 8:41:07 PM","Latitude":"","Longitude":"","CellTowerID":"","ResultCode":0,"ResultDescription":"Transaction Successful","Is_Online":false,"SelectionMode":false,"Is_Confirmed":false,"KickBack":0.0000,"BonusPoint":0,"Comments":"Bank To Wallet , Bank TransactionId : 2349591 Account Number :00959266277759  ","TownshipName":null,"TownshipId":null,"DestinationNumberWalletBalance":0.0000,"RootAgentCode":"00959783798314","CashFlowType":"0"},{"Source":"00959783798314","Destination":"00959973332668","TransactionTypeName":"PAYTO","Amount":10000.0000,"Balance":0.0,"TransactionID":"2009505824","TransactionTime":"2021-06-03T15:50:07","Updated_Date":"6/3/2021 3:49:00 PM","Latitude":"","Longitude":"","CellTowerID":"","ResultCode":0,"ResultDescription":"Transaction Successful","Is_Online":false,"SelectionMode":false,"Is_Confirmed":false,"KickBack":0.0000,"BonusPoint":0,"Comments":"Bank To Wallet , Bank TransactionId : 012106031548544144 Account Number :1234  ","TownshipName":null,"TownshipId":null,"DestinationNumberWalletBalance":229615.0000,"RootAgentCode":"00959783798314","CashFlowType":"0"},{"Source":"00959783798314","Destination":"00959973332668","TransactionTypeName":"PAYTO","Amount":3000.0000,"Balance":0.0,"TransactionID":"2009505830","TransactionTime":"2021-06-03T17:00:29","Updated_Date":"6/3/2021 4:59:22 PM","Latitude":"","Longitude":"","CellTowerID":"","ResultCode":0,"ResultDescription":"Transaction Successful","Is_Online":false,"SelectionMode":false,"Is_Confirmed":false,"KickBack":0.0000,"BonusPoint":0,"Comments":"Bank To Wallet , Bank TransactionId : 012106031659154150 Account Number :1234  ","TownshipName":null,"TownshipId":null,"DestinationNumberWalletBalance":245615.0000,"RootAgentCode":"00959783798314","CashFlowType":"0"},{"Source":"00959783798314","Destination":"00959973332668","TransactionTypeName":"PAYTO","Amount":3000.0000,"Balance":0.0,"TransactionID":"2009505826","TransactionTime":"2021-06-03T15:58:29","Updated_Date":"6/3/2021 3:57:22 PM","Latitude":"","Longitude":"","CellTowerID":"","ResultCode":0,"ResultDescription":"Transaction Successful","Is_Online":false,"SelectionMode":false,"Is_Confirmed":false,"KickBack":0.0000,"BonusPoint":0,"Comments":"Bank To Wallet , Bank TransactionId : 012106031557164146 Account Number :1234  ","TownshipName":null,"TownshipId":null,"DestinationNumberWalletBalance":232615.0000,"RootAgentCode":"00959783798314","CashFlowType":"0"},{"Source":"00959783798314","Destination":"00959262888032","TransactionTypeName":"PAYTO","Amount":10.0000,"Balance":0.0,"TransactionID":"2009506280","TransactionTime":"2021-06-16T20:48:36","Updated_Date":"6/16/2021 8:47:26 PM","Latitude":"","Longitude":"","CellTowerID":"","ResultCode":0,"ResultDescription":"Transaction Successful","Is_Online":false,"SelectionMode":false,"Is_Confirmed":false,"KickBack":0.0000,"BonusPoint":0,"Comments":"Bank To Wallet , Bank TransactionId : 2349592 Account Number :00959262888032  ","TownshipName":null,"TownshipId":null,"DestinationNumberWalletBalance":39029771.0600,"RootAgentCode":"00959783798314","CashFlowType":"0"}]');
         
      // if(respData.length > 0){
      //    for (let index = 0; index < respData.length; index++) {
      //       const element = respData[index];
      //       element.Amount = element.Amount + " MMK";
      //       element.Balance = element.Balance + " MMK";
      //       element.TownshipName = element.TownshipName == null ? 'Unknown':element.TownshipName; 
      //       element.Destination = "(+95)" + element.Destination;
      //    }
      // }

    //  this.rowData = respData, this.rowAllData = respData;
      
      this.assoc.getReport(this.reqData).subscribe((result) => {
         console.warn(result);
         var respData = JSON.parse(JSON.parse(result.Data));

         if(respData.length > 0){
            for (let index = 0; index < respData.length; index++) {
               const element = respData[index];
               element.Amount = element.Amount + " MMK";
               element.Balance = element.Balance + " MMK";
               element.TownshipName = element.TownshipName == null ? 'Unknown':element.TownshipName; 
               element.Destination = "(+95)" + element.Destination;

               // Comments Section
               let Comments = respData[index].Comments;

               if (Comments != "" && Comments.indexOf(",") != -1 && Comments.split(",").length > 2) {
                  respData[index]["Age"] = Comments.split(",")[4];
                  respData[index]["UserName"] = Comments.split(",")[7];
                  respData[index]["BusinessName"] = Comments.split(",")[5] == ""? "-": Comments.split(",")[5];
               
                  switch (Comments.split(",")[3]) {
                     case "1":
                        respData[index]["AccountType"] = "Agent";
                        break;
                     case "2":
                        respData[index]["AccountType"] = "Merchant";
                        break;
                     case "4":
                        respData[index]["AccountType"] = "Advance Merchant";
                        break;
                     case "5":
                        respData[index]["AccountType"] = "Safety Cashier";
                        break;
                     case "6":
                        respData[index]["AccountType"] = "Personal";
                        break;
                     case "8":
                        respData[index]["AccountType"] = "One Stop Mart";
                        break;
                     case "11":
                        respData[index]["AccountType"] = "XXXXXXXXXXXX";
                        break;
                     default:
                        respData[index]["AccountType"] = "Personal";
                        break;
                  }
                  
                  
                  if (respData[index]["UserName"] != null && (respData[index]["UserName"].indexOf("U") != -1 ||
                  respData[index]["UserName"].indexOf("Mg") != -1 ||
                  respData[index]["UserName"].indexOf("Mr") != -1 ||
                  respData[index]["UserName"].indexOf("Dr") != -1 )){
                     respData[index]["Gender"] = "Male";
                  }else {
                     respData[index]["Gender"] = "Female";
                  }
                  
               }else{
                  respData[index]["Age"] = null;   
                  respData[index]["UserName"] = null;
                  respData[index]["Gender"] = null;
                  respData[index]["AccountType"] = "";
                  respData[index]["BusinessName"] = "-";
               }
            }
         }
          this.rowData = respData, this.rowAllData = respData;
       });
   
   
      } catch (error) {
        console.log("Error in NgInit function", error); 
      };  
    }

   applyFilter(event:any) {
      var filteredData:any = [];
      var element = event.target;
      var selFilter = element.closest("ul").getAttribute("name");

      if (selFilter == "filter") {
         if (event.target.value == -1) {
            this.rowData = this.rowAllData;
         } else {
          switch (event.target.value) {
             case 0:
                this.rowData = this.rowAllData;
                break;
                case 1: // Cash back
                   this.rowAllData.forEach(function (data:any) {
                      if (data.KickBack > 0) {
                        filteredData.push(data);
                      }
                   });
                   this.rowData = filteredData;
                  break;
                  case 2: // Remarks
                     this.rowAllData.forEach(function (data:any) {
                        if (data.Comments != '') {
                          filteredData.push(data);
                        }
                     });
                     this.rowData = filteredData;
                break;
                case 3: // Refunds
                  this.rowAllData.forEach(function (data:any) {
                     if (data) {
                       filteredData.push(data);
                     }
                  });
                  this.rowData = filteredData;
                break;
                case 4: // Payment
                  this.rowAllData.forEach(function (data:any) {
                     if (data.Amount > 0) {
                       filteredData.push(data);
                     }
                  });
                  this.rowData = filteredData;
                break;
             default:
                break;
          }  
         }
      }else if (selFilter == "amount") {
         var selOption= parseInt(event.target.value);
         if (selOption == -1) {
            this.rowData = this.rowAllData;
         } else {
            switch (selOption) {
               case 0:
                  this.rowData = this.rowAllData;
                  break;
                  case 1:
                  case 2:
                  case 3:
                  case 4:
                  case 5:
                  case 6:
                  
                     this.rowAllData.forEach(function (data:any) {
                        var amountValue =  parseInt(data.Amount.split('MMK')[0].trim());
                        if (selOption == 1 && amountValue > 0 && amountValue <= 1000) {
                          filteredData.push(data);
                        }
                        else if (selOption == 2 && amountValue > 1000 && amountValue <= 10000) {
                           filteredData.push(data);
                         }
                         else if (selOption == 3 && amountValue > 10000 && amountValue <= 50000) {
                           filteredData.push(data);
                         }
                         else if (selOption == 4 && amountValue > 50000 && amountValue <= 100000) {
                           filteredData.push(data);
                         }
                         else if (selOption == 5 && amountValue > 1000000 && amountValue <= 2000000) {
                           filteredData.push(data);
                         }
                         else if (selOption == 6 && amountValue > 2000000 && amountValue <= 5000000) {
                           filteredData.push(data);
                         }
                     });
                     this.rowData = filteredData;
                  break;
                  
            }
         }
      }else if (selFilter == "gender") {
         debugger;
         if (event.target.value == -1) {
            this.rowData = this.rowAllData;
         } else {
            switch (event.target.value) {
               case 0:
                  this.rowData = this.rowAllData;
                  break;
                  case 1:
                     this.rowAllData.forEach(function (data:any) {
                        if (data.Gender != null && data.Gender == "Male") {
                          filteredData.push(data);
                        }
                     });
                     this.rowData = filteredData;
                  break;
                  case 2:
                     this.rowAllData.forEach(function (data:any) {
                        if (data.Gender != null && data.Gender == "Male") {
                          filteredData.push(data);
                        }
                     });
                     this.rowData = filteredData;
                  break;
            
            }
         }
      }else if (selFilter == "age"){
         if (event.target.value == -1) {
            this.rowData = this.rowAllData;
         } else {
            switch (event.target.value) {
               case 0:
                  this.rowData = this.rowAllData;
                  break;
                  case 1:
                     this.rowAllData.forEach(function (data:any) {
                        if (data.Age != null && data.Age <= 19) {
                          filteredData.push(data);
                        }
                     });
                     this.rowData = filteredData;
                  break;
                  case 2:
                     this.rowAllData.forEach(function (data:any) {
                        if (data.Age != null && (data.Age > 19 || data.Age <= 34)) {
                          filteredData.push(data);
                        }
                     });
                     this.rowData = filteredData;
                  break;
                  case 3:
                     this.rowAllData.forEach(function (data:any) {
                        if (data.Age != null && (data.Age > 34 || data.Age <= 50)) {
                          filteredData.push(data);
                        }
                     });
                     this.rowData = filteredData;
                  break;
                  case 4:
                     this.rowAllData.forEach(function (data:any) {
                        if (data.Age != null && (data.Age > 50 || data.Age <= 64)) {
                          filteredData.push(data);
                        }
                     });
                     this.rowData = filteredData;
                  break;
                  case 5:
                     this.rowAllData.forEach(function (data:any) {
                        if (data.Age != null && data.Age > 64 ) {
                          filteredData.push(data);
                        }
                     });
                     this.rowData = filteredData;
                  break;
            
            }
         }
      }
      else if (selFilter == "period"){
         switch (event.target.value) {
            case 0:
               this.rowData = this.rowAllData;
               break;
            case 1:
               this.openDatePickerDialog();
               break;
         }
      }

      // hiding menu
      element.closest("ul").classList.add("hide");
   }

   // applyDateRangeFilter{

   // }
   onDateSelection(date: NgbDate) {
      if (!this.fromDate && !this.toDate) {
        this.fromDate = date;
      } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
        this.toDate = date;
        this.closeDialog(null);
        var that = this; 
        var filteredData:any = [], fromDt = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day), toDt = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day);
        this.rowAllData.forEach(function (data:any) {
           var dataDate = new Date(data.Updated_Date);
         if (dataDate.getTime() >=  fromDt.getTime() &&   dataDate.getTime() <=  toDt.getTime()) {
            filteredData.push(data);
         }
      });
      this.rowData = filteredData;

      } else {
        this.toDate = null;
        this.fromDate = date;
      }
    }
  
    isHovered(date: NgbDate) {
      return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
    }
  
    isInside(date: NgbDate) {
      return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
    }
  
    isRange(date: NgbDate) {
      return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
    }

    openDatePickerDialog(){
      document.getElementById("datePickerModel")?.classList.remove("hide");
   }

   showHideFilterMenu(event:any) {
      var element = event.target;
      var selMenu = element.closest(".tollbar-filter");
      if (!selMenu.children[1].classList.contains("hide")) {
         selMenu.children[1].classList.add("hide");
      }else{
         var menuFilters = document.getElementsByClassName("filter-menu");
         for (let index = 0; index < menuFilters.length; index++) {
            menuFilters[index].classList.add("hide");
         }
         
         selMenu.children[1].classList.remove("hide");
      }
      
   }

   onGridReady(params: any) {
      this.gridApi = params.api;

   //    setTimeout(()=>{                           
   //       debugger;
   //       var el = document.querySelector('.export-icon');
   //       if(el) {
   //         el.addEventListener('click', this.exportToExcel.bind(this));
   //       }
   //   }, 3000);
   }

   onCellClicked(data: any){
      var selRowData = data.data;
      this.value = selRowData.Comments;
      if(data.event.target.name == 'qrcode'){
         document.getElementById("qrCodeModel")?.classList.remove("hide");
      } else if ( data.event.target.name ==  'exportpdf') {
         this.pdfTransactionId = selRowData.TransactionID;
         this.pdfTransactionType = selRowData.TransactionTypeName;
         this.pdfSenderAccountName = "";
         this.pdfSenderAccountNumber = selRowData.Source;
         this.pdfReceiverAccountName = "";
         this.pdfReceiverAccountNumber = selRowData.Destination;
         this.pdfDateTime = selRowData.Updated_Date;
         this.pdfAmount = selRowData.Amount;
         
         this.downloadAsPDF();
      }

   }

   closeDialog(data:any){
      document.getElementById("qrCodeModel")?.classList.add("hide");
      document.getElementById("shareDataModel")?.classList.add("hide");
      document.getElementById("searchDataModel")?.classList.add("hide");
      document.getElementById("datePickerModel")?.classList.add("hide");
   }

   openShareDataDialog(data:any){
      document.getElementById("shareDataModel")?.classList.remove("hide");
   }

   openSearchDataDialog(data:any){
      document.getElementById("searchDataModel")?.classList.remove("hide");
   }

   exportAllToPDF(){
      document.getElementById("htmlData")?.classList.remove("hide");
      document.getElementById("transactionData")?.classList.remove("hide");
      document.getElementById("loader")?.classList.remove("hide");


      //this.USERDETAILS =  [];
      // this.USERDETAILS.push({key1:"Account Name",val1:"Mr, Krishna Moorthy", key2:"Period",val2:"All"});
      // this.USERDETAILS.push({key1:"Account Number",val1:"+95 09798039885", key2:"Total Cashback Amount",val2:"550 MMK"});
      // this.USERDETAILS.push({key1:"E-mail",val1:" krishnamoorthytaurus@gmail.com ,krishnamoorthy@goautoticket .com", key2:"Total Credit Amount",val2:"0 MMK"});
      // this.USERDETAILS.push({key1:"Account Type",val1:"Personal", key2:"Total Debit Amount",val2:"435,000 MMK"});
      // this.USERDETAILS.push({key1:"Address",val1:" No (7) Ward, Kamayut Township, Yangon, Yangon Division", key2:"Total Transaction Amount",val2:"435,000 MMK"});
      // this.USERDETAILS.push({key1:"",val1:"", key2:"Closing Balance",val2:"2,666 MMK"});
      // this.USERDETAILS.push({key1:"",val1:"", key2:"Date & Time",val2:"Thu, 02-Sep-2021 11:34:22"});
      // this.USERDETAILS.push({key1:"",val1:"", key2:"Total Transactions",val2:"5"});

      this.pdfData = [];
      this.rowData.forEach(element => {
         
         this.pdfData.push(element);
      });
      
      let PDF = new jsPDF('l', 'mm', 'a4');
      let DATA:any = document.getElementById('htmlData');
      let SecondPageData:any = document.getElementById('transactionData');
      let fileWidth = 295;
      let fileHeight = 100;

      html2canvas(DATA).then(canvas => {
         let fileWidth = 295;
         let fileHeight = canvas.height * fileWidth / canvas.width;
         
         const FILEURI = canvas.toDataURL('image/png')
         let PDF = new jsPDF('l', 'mm', 'a4');
         let position =  85;
         PDF.addImage("../../assets/Images/logo.png", "PNG", 125, 15, 50, 50);
         PDF.setFontSize(22);
         
         PDF.setTextColor("black");
         PDF.setFontSize(18);
         PDF.text("All - Transaction Detail", 20, 100);
         
         //PDF.setFontSize(30);
         PDF.addImage(FILEURI, 'PNG', 20, position, fileWidth, fileHeight);
         PDF.setTextColor("blue");
         PDF.text("Transaction Detail Statement", 100, 80);

         html2canvas(SecondPageData).then(canvas2 => {
            const   FILEURI = canvas2.toDataURL('image/png');
            PDF.addPage();
            PDF.addImage(FILEURI, 'PNG', 20, 20, fileWidth, fileHeight);
            

            const pageCount = PDF.getNumberOfPages();

            // For each page, print the page number and the total pages
            for(var i = 1; i <= pageCount; i++) {
               // Go to page i
               PDF.setPage(i);
               //Print Page 1 of 4 for example
               PDF.setFontSize(10);
               PDF.text('Page ' + String(i) + ' of ' + String(pageCount),220,10);
            }


               PDF.save('OK-Dollar All Transactions Report.pdf');

               document.getElementById("htmlData")?.classList.add("hide");
               document.getElementById("transactionData")?.classList.add("hide");
               document.getElementById("loader")?.classList.add("hide");
               document.getElementById("shareDataModel")?.classList.add("hide");
      });
     });    


      // html2canvas(DATA).then(canvas => {
      //     fileHeight = canvas.height * fileWidth / canvas.width;
          
      //     const FILEURI = canvas.toDataURL('image/png')
          
      //     let position =  85;
      //     PDF.addImage("../../assets/Images/logo.png", "PNG", 125, 15, 50, 50);
      //     PDF.setFontSize(22);
          
      //     PDF.setTextColor("black");
      //     PDF.setFontSize(18);
      //     PDF.text("All - Transaction Detail", 20, 100);
          
      //     //PDF.setFontSize(30);
      //     PDF.addImage(FILEURI, 'PNG', 20, position, fileWidth, fileHeight);
      //     const pageCount = PDF.getNumberOfPages();

      //     // For each page, print the page number and the total pages
      //     for(var i = 1; i <= pageCount; i++) {
      //        // Go to page i
      //        PDF.setPage(i);
      //        //Print Page 1 of 4 for example
      //        PDF.setFontSize(10);
      //        PDF.text('Page ' + String(i) + ' of ' + String(pageCount),220,10);
      //     }
     
     
      //      PDF.save('OK-Dollar All Transactions Report.pdf');    
      // });    

   //    html2canvas(SecondPageData).then(canvas => {
   //    //  fileHeight = canvas.height * fileWidth / canvas.width;
      
   //    // PDF.addPage();
   //    // const FILEURI = canvas.toDataURL('image/png');
   //    // PDF.addImage(FILEURI, 'PNG', 20, 20, fileWidth, fileHeight);
          
   //    const pageCount = PDF.getNumberOfPages();

   //   // For each page, print the page number and the total pages
   //   for(var i = 1; i <= pageCount; i++) {
   //      // Go to page i
   //      PDF.setPage(i);
   //      //Print Page 1 of 4 for example
   //      PDF.setFontSize(10);
   //      PDF.text('Page ' + String(i) + ' of ' + String(pageCount),220,10);
   //   }


   //    PDF.save('OK-Dollar All Transactions Report.pdf');
   // });
   }
   exportToExcel(selElement: any) {
    this.gridApi.exportDataAsCsv();
  }
  public downloadAsPDF() {
   const doc = new jsPDF();

   const specialElementHandlers = {
     '#editor': function (element:any, renderer:any) {
       return true;
     }
   };

   doc.addImage("../../assets/Images/logo.png", "PNG", 75, 15, 50, 50);
   doc.setFontSize(20);
   doc.setTextColor("blue");
   doc.text("Transaction Details Receipt", 55, 80);
   doc.setTextColor("black");
   doc.setFontSize(18);
   doc.setLineHeightFactor(2);
   var text = "Sender Account Name : "+ this.pdfSenderAccountName + "\n" +
   "Sender Account Number : "+ this.pdfSenderAccountNumber + "\n" +
   "Receiver Account Name : "+ this.pdfReceiverAccountName + "\n" +
   "Receiver Account Number : "+ this.pdfReceiverAccountNumber + "\n" +
   "Trancation ID : "+ this.pdfTransactionId + "\n" +
   "Trancation Type : "+ this.pdfTransactionType + "\n" +
   "Date & Time : "+ this.pdfDateTime + "\n\n" +
   
   "Amount Paid : "+ this.pdfAmount + "\n\n" ;
   doc.text(text, 35, 100);
   let barcdeImage = document.getElementsByClassName("bshadow");

   if (barcdeImage[0].firstElementChild != null ) {
      let imageSrc:any = barcdeImage[0].firstElementChild.getAttribute("src");
      doc.addImage(imageSrc, "PNG",75, 220, 50, 50);
      doc.addImage("../../assets/Images/ok_logo.png", "PNG", 95, 240, 10, 10);   
   }
   
   doc.save('OK-Dollar Transaction Report.pdf');
//    doc.html(pdfTable.innerHTML, {
//    callback: function (doc) {
//       doc.setFont("helvetica");
//      doc.save('ExportDetails.pdf');
//    },
//    x: 10,
//    y: 10
// });
 }

}