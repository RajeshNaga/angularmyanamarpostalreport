import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';



var headers_object = new HttpHeaders({
  'Content-Type': 'application/json',
  
});

const httpOptions = {	

  headers:headers_object
  
};

@Injectable({
  providedIn: 'root'
})


export class AssociationService {
 // url_= "http://103.47.184.246:8001/RestService.svc/"
  constructor(private httpClient: HttpClient) { }


  getReport(datam: any) :Observable<any>{
    return this.httpClient.post<any>("/RestService.svc/GetTransInfoByAgentCode", datam, {headers: 
      {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } })
  }

}
